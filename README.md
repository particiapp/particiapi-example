# Particiapp Example Frontend

This project contains an example frontend for Polis using the Particiapi
server. A protoype for a Particiapi client library is also included in this
project (see `scripts/particiapi-client.js`).
